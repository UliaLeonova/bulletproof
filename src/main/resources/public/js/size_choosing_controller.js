angular.module('bulletproofControllers')

    .controller('sizeChoosingController', ['$scope', 'productService',
        function ($scope, productService) {
            $scope.height;
            $scope.bust;
            $scope.waist;
            $scope.hip;
            var Size = function(minHeight, maxHeight, minBust, maxBust, minWaist, maxWaist,minHip, maxHip, internationalSize,russianSize) {
                this.maxHeight = maxHeight;
                this.minHeight = minHeight;
                this.maxBust =maxBust;
                this.minBust = minBust;
                this.maxWaist = maxWaist;
                this.minWaist = minWaist;
                this.minHip = minHip;
                this.maxHip = maxHip;
                //this.euroSize= euroSize;
                this.internationalSize = internationalSize
                this.russianSize = russianSize;

            };
            //var euroSize;
            var internationalSize;
            var russianSize;

            Size.prototype.isApplicable = function(height,bust,waist,hip) {
                if ((height>=this.minHeight&&height<=this.maxHeight)&&
                    (hip >= this.minHip && hip <= this.maxHip)&&
                    (bust>=this.minBust&&bust<=this.maxBust)&&
                    (waist>=this.minWaist&& waist<=this.maxWaist)) {
                    return {
                       // euro:this.euroSize,
                        international: this.internationalSize,
                        russian: this.russianSize
                    };
                }
            };
            var arr=[];
            arr.push(new Size(0,190,74,80,60,65,84,90,'xs',40 ));
            arr.push(new Size(0,190,81,85,66,69,91,95,'xs',42));
            arr.push(new Size(0,190,86,89,70,73,96,98,'s',44));
            arr.push(new Size(0,190,90,93,74,77,99,101,'m',46));
            arr.push(new Size(0,190,94,97,78,81,102,104,'m',48));
            arr.push(new Size(0,190,98,102,82,85,105,108,'l',50));


            var validate = function() {
                var result = true;
                if (!$scope.size.heightInput.$valid
                ||!$scope.size.bustInput.$valid
                ||!$scope.size.waistInput.$valid
                ||!$scope.size.hipsInput.$valid

                ) {
                    result = false;
                }

                return result;
            };


            $scope.choose = function() {

                if (!validate()) {
                    $scope.message = 'Пожалуйста, введите корректные данные';
                    $scope.err = true;
                    return;
                }

                var size;
                for (var i = 0; i < arr.length; i++) {
                    var result = arr[i].isApplicable($scope.height, $scope.bust, $scope.waist, $scope.hip);
                    if (result) {
                        size = result;
                    }
                }
                if (size) {
                    $scope.message='Оптимальный для Вас размер: \r\n'
                        + '\r\n Международный: ' + size.international + '\r\n Российкий: ' + size.russian;
                    $scope.err=false;
                } else {
                    $scope.message='К сожалению, мы не можем точно определить размер по вашим параметрам. ' +
                        'Пожалуйста, обратитесь к нашим консультантам';
                    $scope.err=true;
                }
            }



        }

    ]);

/**
 * Created by Yuliya on 1/8/2016.
 */
