angular.module('bp.directives', ['angularModalService'])
    .directive('bpSlider', function () {
        return {
            restrict: 'E',
            scope: {
                images: '=',
                showCount: '='
            },
            controller: ['$scope', function ($scope) {
                $scope.activeSlides = $scope.images.slice(0, $scope.showCount);
                $scope.startSlide = 0;

                $scope.counter = [];
                for (var i = 1; i < $scope.images.length; i++) {
                    $scope.counter.push(i);
                }


                $scope.nextSlide = function () {

                    if ($scope.startSlide + $scope.showCount <= $scope.images.length) {
                        $scope.startSlide++;
                        $scope.setSlide($scope.startSlide + 1);
                    }
                };

                $scope.prevSlide = function () {

                    if ($scope.startSlide > 0) {
                        $scope.startSlide--;
                        $scope.setSlide($scope.startSlide + 1);
                    }
                };

                $scope.setSlide = function (index) {
                    $scope.startSlide = index - 1;
                    if (index > 0 && index < $scope.images.length - 1) {
                        $scope.activeSlides = $scope.images.slice($scope.startSlide,
                            $scope.startSlide + $scope.showCount);
                    } else if (index == 0) {

                        $scope.activeSlides = [$scope.images[$scope.images.length - 1]]
                            .concat($scope.images.slice(0, $scope.showCount - 1));
                    } else {
                        $scope.activeSlides =
                            $scope.images.slice($scope.startSlide, $scope.startSlide + $scope.showCount - 1);
                        $scope.activeSlides.push($scope.images[0]);
                    }
                }
            }],
            templateUrl: 'directives/slider.html'
        };
    })
    .directive('header', function () {
        return {
            restrict: 'A', //This menas that it will be used as an attribute and NOT as an element. I don't like creating custom HTML elements
            replace: true,
            templateUrl: "/directives/header.html",
            controller: ['$scope', '$filter', 'ModalService', 'loginService', function ($scope, $filter, ModalService, loginService) {
                loginService.currentUser().success(function (data) {
                    $scope.currUser = data;
                });

                $scope.login = function () {
                    ModalService.showModal({
                        templateUrl: "/template/login_dialog.html",
                        controller: "loginController"
                    }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {
                            if (result == 'reg') {
                                $scope.showRegistration()
                            }
                            if (result) {
                                loginService.currentUser().success(function (data) {
                                    $scope.currUser = data;
                                });
                            }

                        });
                    });
                };

                $scope.logout = function () {
                    loginService.logout();
                    $scope.currUser = null;
                };

                $scope.showCertificate = function () {
                    ModalService.showModal({
                        templateUrl: "/template/certificate.html",
                        controller: "ModalController"
                    }).then(function (modal) {
                        modal.element.modal();
                    });
                };

                $scope.showRegistration = function () {
                    ModalService.showModal({
                        templateUrl: "/template/registration_dialog.html",
                        controller: "registrationController"
                    }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {
                            if (result) {
                                loginService.currentUser().success(function (data) {
                                    $scope.currUser = data;
                                });
                            }

                        });
                    });

                }
            }]
        }
    })
    .directive('products', function () {
        return {
            restrict: 'A', //This menas that it will be used as an attribute and NOT as an element. I don't like creating custom HTML elements
            replace: true,
            templateUrl: "/directives/products.html", scope: {
                collection: '='
            },
            controller: ['$scope', 'productService','ModalService', function ($scope, productService,ModalService) {
                $scope.availablePageSizes = [15, 25, 50, 100];
                $scope.pageSize = 15;
                $scope.pageNum = 0;

                $scope.sortOptions = [
                    {
                        label: 'Цене по убыванию',
                        field: 'price',
                        order: 'DESC'
                    },
                    {
                        label: 'Цене по возрастанию',
                        field: 'price',
                        order: 'ASC'
                    },
                    {
                        label: 'По новизне',
                        field: 'createdDate',
                        order: 'ASC'
                    }
                ];

                var updateProducts = function () {
                    var products_promise = productService
                        .getProducts($scope.collection,
                            $scope.category,
                            $scope.sortBy,
                            $scope.sortOrder,
                            $scope.filterType,
                            $scope.pageSize,
                            $scope.pageNum
                        );
                    products_promise.success(function (data) {
                        $scope.products = data;
                    });
                };

                updateProducts();
                var sizes_promise = productService.getAllSizes();
                sizes_promise.success(function (data) {
                    $scope.sizes = data;
                });

                var categories_promise = productService.getAllCategories();
                categories_promise.success(function (data) {
                    $scope.categories = data;
                });

                $scope.filter = function (size) {
                    $scope.filterType = size;
                    $scope.pageNum = 0;
                    updateProducts()
                };
                $scope.nextPage = function () {
                    $scope.pageNum++;
                    updateProducts()
                };
                $scope.prevPage = function () {
                    if ($scope.pageNum > 0) {
                        $scope.pageNum--;
                        updateProducts();
                    }
                };
                $scope.sort = function (sortBy, sortOrder) {
                    $scope.pageNum = 0;
                    $scope.sortBy = sortBy;
                    $scope.sortOrder = sortOrder;
                    updateProducts();
                };

                $scope.setPageSize = function (size) {
                    $scope.pageNum = 0;
                    $scope.pageSize = size;
                    updateProducts();
                };

                $scope.changeCategory = function (category) {
                    $scope.category = category;
                    updateProducts();
                };
                $scope.showProduct = function (product) {
                    ModalService.showModal({
                        templateUrl: "/template/product_dialog.html",
                        controller: "ProductDialogController",
                        inputs: {
                            product_id:product.id,
                            product:product
                        }
                    }).then(function (modal) {
                        modal.element.modal();
                    });
                }

            }]
        }
    });
