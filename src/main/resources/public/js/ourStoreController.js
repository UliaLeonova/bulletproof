/**
 * Created by Yuliya on 4/1/2016.
 */
angular.module('bulletproofControllers')

    .controller('ourStoreController', ['$scope','sliderService',

    function ($scope, sliderService) {
        //var products_promise = productService.getProducts();
        //products_promise.success(function (data) {
        //    $scope.products = data
        //});

        $scope.slider = {};
        $scope.slider.images = sliderService.getSlidesOurStore();


    }

]);
