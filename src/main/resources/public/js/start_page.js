angular.module('bulletproofControllers', ['bulletproofServices'])

    .controller('startPageController', ['$scope', 'productService', 'sliderService','Page',
        function ($scope, productService, sliderService, Page) {
            //var products_promise = productService.getProducts();
            //products_promise.success(function (data) {
            //    $scope.products = data
            //});

            $scope.slider = {};
            $scope.slider.images = sliderService.getAllSlides();
            $scope.login = function () {
                Page.login();
            }

        }


    ]).factory('Page', function () {
    var title = 'default';
    return {
        title: function () {
            return title;
        },
        setTitle: function (newTitle) {
            title = newTitle
        },
        login: function () {
            ModalService.showModal({
                templateUrl: "templates/login_dialog.html",
                controller: "YesNoController"
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    $scope.yesNoResult = result ? "You said Yes" : "You said No";
                });
            });
        }
    };
});

           