/**
 * Created by Yuliya on 3/31/2016.
 */

var app = angular.module('bulletproof');
app.controller('actionController', ['$scope','ModalService',function ($scope,ModalService ) {
    $scope.showAction = function (templateUrl) {
        ModalService.showModal({
            templateUrl: templateUrl,
            controller: "ModalController"
        }).then(function (modal) {
            modal.element.modal();
        });
    };


}]);