var app = angular.module('bulletproof');

app.controller('ModalController', function ($scope, close) {

    // when you need to close the modal, call close
    $scope.close = function(result) {
        close(result, 500); // close, but give 500ms for bootstrap to animate
    };
});