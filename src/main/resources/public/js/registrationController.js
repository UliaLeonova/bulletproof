var app = angular.module('bulletproof');

app.controller('registrationController', ['$scope', '$element', 'close', 'loginService', function ($scope, $element, close, loginService) {

    $scope.close = function (result) {
        close(result, 500); // close, but give 500ms for bootstrap to animate
    };

    $scope.register = function () {
        loginService.register($scope.userName, $scope.email, $scope.password, $scope.phone, $scope.birthdate)
            .success(function (data) {
                if (data.success == true) {
                    //  Manually hide the modal.
                    $element.modal('hide');
                    $scope.close(true);
                } else {
                    $scope.message = data.message;
                }
            });
    }

}]);