var Size = function(minHeight, maxHeight, minBust, maxBust, minWaist, maxWaist,minHip, maxHip, euroSize, internationalSize,russianSize) {
    this.maxHeight = maxHeight;
    this.minHeight = minHeight;
    this.maxBust =maxBust;
    this.minBust = minBust;
    this.maxWaist = maxWaist;
    this.minWaist = minWaist;
    this.minHip = minHip;
    this.maxHip = maxHip;
    this.euroSize= euroSize;
    this.internationalSize = internationalSize
    this.russianSize = russianSize;

};
var euroSize;
var internationalSize;
var russianSize;

Size.prototype.isApplicable = function(height,bust,waist,hip) {
    if ((height>this.minHeight&&height<this.maxHeight)&&
        (hip > this.minHip && hip < this.maxHip)&&
        (bust>this.minBust&&bust<this.maxBust)&&
        (waist>this.minWaist&& waist<this.maxWaist)) {
        return {
            euro:this.euroSize,
            international: this.internationalSize,
            russian: this.russianSize
        };
    }
};
var arr=[];
arr.push(new Size(0,165,74,80,60,65,84,90,16, 'xs',40 ));
arr.push(new Size(166,171,74,80,60,65,84,90, 32, 'xs',40));
arr.push(new Size(0,165,81,85,66,69,91,95, 17,'xs',42));
arr.push(new Size(166,171,81,85,66,69,91,95,34, 'xs', 42));
arr.push(new Size(172,190,81,85,66,69,91,95,68, 'xs',42));
arr.push(new Size(0,165,86,89,70,73,96,98,18,'s',44));
arr.push(new Size(166,171,86,89,70,73,96,98,36,'s',44));
arr.push(new Size(172,190,86,89,70,73,96,98,72,'s',44));
arr.push(new Size(0,165,90,93,74,77,99,101,19,'m',46));
arr.push(new Size(166,171,90,93,74,77,99,101,38,'m',46));
arr.push(new Size(172,190,90,93,74,77,99,101,76,'m',46));
arr.push(new Size(0,165,94,97,78,81,102,104,20,'m',48));
arr.push(new Size(166,171,94,97,78,81,102,104, 40,'m',48));
arr.push(new Size(172,190,94,97,78,81,102,104, 80,'m',48));
arr.push(new Size(0,165,98,102,82,85,105,108,21,'l',50));
arr.push(new Size(166,171,98,102,82,85,105,108,42,'l',50));
arr.push(new Size(172,190,98,102,82,85,105,108, 84,'l',50));




for(var i = 0; i<arr.length; i++){
    var result = arr[i].isApplicable(178,99,84,106);
    if(result){
        console.log(result);
    }

}

