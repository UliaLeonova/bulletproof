var bulletproofApp = angular.module('bulletproof', ['ngRoute', 'bulletproofControllers', 'bp.directives', 'angularModalService']);
bulletproofApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.
    when('/', {
        templateUrl: 'template/start.html',
        controller: 'startPageController'
    }).
    when('/new', {
        templateUrl: 'template/new-things.html',
        controller: 'newThingsController'
    }).
    when('/size', {
        templateUrl: 'template/size_choosing.html',
        controller: 'sizeChoosingController'
    }).
    when('/lookbook', {
        templateUrl: 'template/lookbook.html'

    }).
    when('/blog', {
        templateUrl: 'template/blog.html'

    }).
    when('/sale', {
        templateUrl: 'template/sale.html',
        controller: 'saleController'
    }).
    when('/contacts', {
        templateUrl: 'template/contacts.html'

    }).

    when('/shop', {
        templateUrl: 'template/shop.html',
        controller: 'shopController'
    }).
    when('/help', {
        templateUrl: 'template/help.html'
    }).
    when('/lookbookInside', {
        templateUrl: 'template/lookbook_inside.html',
        controller: 'shopController'
    }).
    when('/ourStore', {
        templateUrl: 'template/ourStore.html',
            controller: 'ourStoreController'

    }).
    when('/collections/:collection', {
        templateUrl: 'template/collection.html',
        controller: 'collectionController'

    }).
        when('/actions', {
            templateUrl: 'template/actions.html',
            controller: 'actionController'

        }).

        when('/basket', {
            templateUrl: 'template/basket.html',
            controller: 'basketController'

        }).

    otherwise({
        redirectTo: '/'
    });


}]);