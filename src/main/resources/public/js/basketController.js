angular.module('bulletproofControllers')

    .controller('basketController', ['$scope', 'basketService',
        function ($scope, basketService) {


            basketService.getBasket().success(function (basket) {
                $scope.basket = basket;
                calculatePrice();
            });
            $scope.removeProduct = function (productId) {
                $scope.basket.products = $scope.basket.products.filter(function (element) {
                    return element.id != productId;
                });
                calculatePrice();
                basketService.removeProduct(productId);
            };

            function calculatePrice() {
                var totalPrice = 0;
                if ($scope.basket.products) {
                    $scope.basket.products.forEach(function (element) {
                        totalPrice += element.salePrice ? element.salePrice : element.price;
                    });
                }

                $scope.totalPrice = totalPrice;
            }

        }


    ]);

