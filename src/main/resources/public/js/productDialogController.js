var app = angular.module('bulletproof');

app.controller('ProductDialogController', function ($scope, product, close, basketService) {

    $scope.product = product;
    $scope.inBasket = false;
    basketService.getBasket().success(function (basket) {
        if (basket.products) {
            basket.products.forEach(function (element) {
                if (element.id == product.id) {
                    $scope.inBasket = true;
                }
            });
        }
    });

    // when you need to close the modal, call close
    $scope.close = function (result) {
        close(result, 500); // close, but give 500ms for bootstrap to animate
    };

    $scope.addToBasket = function () {
        basketService.addProduct(product.id);
        $scope.inBasket = true;
    }
});