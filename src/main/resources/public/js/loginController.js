var app = angular.module('bulletproof');

app.controller('loginController', ['$scope', '$element','close','loginService', function($scope, $element,close, loginService) {

    $scope.userName = "";
    $scope.password = "";
    $scope.phone = "";
    $scope.birthdate = "";
    $scope.message;
    $scope.close = function(result) {
        close(result, 500); // close, but give 500ms for bootstrap to animate
    };

    $scope.login = function() {
        loginService.login($scope.userName, $scope.password).success(function (data) {
            if (data.success==true) {
                //  Manually hide the modal.
                $element.modal('hide');
                $scope.close(true);
            } else {
                $scope.message = data.message;
            }
        });
    };


}]);