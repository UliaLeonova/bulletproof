angular.module('bulletproofServices', [])
    .factory('productService', ['$http', function ($http) {
        return {
            getProduct: function (id) {
                //TODO implement
            },
            getProducts: function (collection,category, sortBy, sortOrder, filter, pageSize, pageNum) {
                var url = 'http://localhost:8080/api/products/' + collection;
                return $http.get(url, {
                    params: {
                        categoryId: category,
                        sortBy: sortBy,
                        sortOrder: sortOrder,
                        filter: filter,
                        pageSize: pageSize,
                        pageNum: pageNum
                    }
                });
            },

            getNewProducts: function (category, sortBy, sortOrder, filter, pageSize, pageNum) {
                var url = 'http://localhost:8080/api/products/new/';


                return $http.get(url, {
                    params: {
                        categoryId: category,
                        sortBy: sortBy,
                        sortOrder: sortOrder,
                        filter: filter,
                        pageSize: pageSize,
                        pageNum: pageNum
                    }
                });
            },
            getSaleProducts: function (category, sortBy, sortOrder, filter, pageSize, pageNum) {
                var url = 'http://localhost:8080/api/products/sale/';


                return $http.get(url, {
                    params: {
                        categoryId: category,
                        sortBy: sortBy,
                        sortOrder: sortOrder,
                        filter: filter,
                        pageSize: pageSize,
                        pageNum: pageNum
                    }
                });
            },

            getAllSizes: function () {
                return $http.get('http://localhost:8080/api/products/size');
            },

            getAllCategories: function () {
                return $http.get('http://localhost:8080/api/products/category');
            }

        }
    }])
    .factory('sliderService', ['$http', function ($http) {
        return {
            getAllSlides: function () {
                return ['../product_imgs/product_8.png',
                    '../product_imgs/product_9.png',
                    '../product_imgs/product_10.png',
                    '../product_imgs/product_11.png',
                    '../product_imgs/product_12.png',
                    '../product_imgs/product_16.png',
                    '../product_imgs/product_14.png',
                    '../product_imgs/product_15.png']
            },
            getSlidesOurStore: function(){
                return['../store_imgs/7s.jpg',
                    '../store_imgs/1s.jpg',
                    '../store_imgs/2s.jpg',
                    '../store_imgs/3s.jpg',
                    '../store_imgs/4s.jpg',
                    '../store_imgs/5s.jpg',
                    '../store_imgs/6s.jpg',
                    '../store_imgs/7s.jpg']


            }
        }
    }])
    .factory('loginService', ['$http', function ($http) {
        return {
            login: function (login, password) {
                return $http.get('http://localhost:8080/api/user/login', {
                    params: {
                        email: login,
                        password: password
                    }
                });
            },
            logout: function () {
                return $http.post('http://localhost:8080/api/user/logout');
            },

            register: function (name, email, password, phone, birthDate) {
                return $http.post('http://localhost:8080/api/user/register', {
                    userName: name,
                    email: email,
                    password: password,
                    phone: phone,
                    birthDate: birthDate
                });
            },

            currentUser: function () {
                return $http.get('http://localhost:8080/api/user/current');
            }


        }
    }])
    .factory('basketService', ['$http', function ($http) {
        return {
            addProduct: function (productId) {
                return $http.get('http://localhost:8080/api/basket/add', {
                    params: {
                        productId: productId
                    }
                });
            },

            removeProduct: function (productId) {
                return $http.get('http://localhost:8080/api/basket/remove', {
                    params: {
                        productId: productId
                    }
                });
            },
            getBasket: function () {
                return $http.get('http://localhost:8080/api/basket/' );
            }



        }
    }])
;