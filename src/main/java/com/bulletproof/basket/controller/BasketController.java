package com.bulletproof.basket.controller;

import com.bulletproof.basket.domain.Basket;
import com.bulletproof.basket.service.BasketService;
import com.bulletproof.user.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/basket")
public class BasketController {

    @Autowired
    private BasketService basketService;

    @RequestMapping("/")
    public Basket getBasket() {
        return basketService.getBasket();
    }

    @RequestMapping("/add")
    public void addProduct(@RequestParam Long productId) {
        basketService.addProduct(productId);
    }

    @RequestMapping("/remove")
    public void removeProduct(@RequestParam Long productId) {
        basketService.removeProduct(productId);
    }

}
