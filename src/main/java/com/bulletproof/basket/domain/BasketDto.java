package com.bulletproof.basket.domain;

import com.bulletproof.product.domain.ProductDto;

import java.util.ArrayList;
import java.util.List;

public class BasketDto {
    private List<ProductDto> products = new ArrayList<>();


    public void addProduct(ProductDto product) {
        products.add(product);
    }

    public void removeProduct(ProductDto product) {
        products.remove(product);
    }

    public List<ProductDto> getProducts() {
        return products;
    }
}
