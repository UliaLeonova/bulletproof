package com.bulletproof.basket.domain;

import com.bulletproof.product.domain.ProductDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Basket {

    private List<ProductDto> products = new ArrayList<>();

    public void addProduct(ProductDto product) {
        products.add(product);
    }

    public void removeProduct(ProductDto product) {
        products = products
                .stream()
                .filter(productDto -> !Objects.equals(productDto.getId(), product.getId()))
                .collect(Collectors.toList());
    }

    public List<ProductDto> getProducts() {
        return products;
    }

}
