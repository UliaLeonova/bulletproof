package com.bulletproof.basket.service;

import com.bulletproof.basket.domain.Basket;

public interface BasketService {

    void addProduct(Long productId);
    void removeProduct(Long productId);
    Basket getBasket();
}
