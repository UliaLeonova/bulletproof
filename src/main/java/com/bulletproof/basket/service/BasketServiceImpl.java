package com.bulletproof.basket.service;

import com.bulletproof.basket.domain.Basket;
import com.bulletproof.product.domain.Product;
import com.bulletproof.product.domain.ProductDto;
import com.bulletproof.product.service.ProductService;
import com.bulletproof.user.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BasketServiceImpl implements BasketService {

    @Autowired
    private LoginService loginService;
    @Autowired
    private ProductService productService;

    @Override
    public void addProduct(Long productId) {
        if (loginService.getBasket() != null) {
            Product product = productService.findProduct(productId);
            ProductDto dto = new ProductDto(product);
            loginService.getBasket().addProduct(dto);
        }
    }

    @Override
    public void removeProduct(Long productId) {
        if (loginService.getBasket() != null) {
            Product product = productService.findProduct(productId);
            ProductDto dto = new ProductDto(product);
            loginService.getBasket().removeProduct(dto);
        }
    }

    @Override
    public Basket getBasket() {
        return loginService.getBasket();
    }
}
