package com.bulletproof.user.controller;

import com.bulletproof.user.LoginService;
import com.bulletproof.user.domain.RegistrationRequest;
import com.bulletproof.user.repository.UserRepository;
import com.bulletproof.user.domain.LoginResult;
import com.bulletproof.user.domain.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalField;
import java.util.Date;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private LoginService loginService;

    @RequestMapping("/login")
    public LoginResult login(@RequestParam String email,
                             @RequestParam String password) {
        User usr = userRepository.findByEmail(email);
        if (usr == null) {
            return new LoginResult(false, "Такого пользователя не существует");
        } else {
            if (password.equals(usr.getPassword())) {
                loginService.login(usr);
                return new LoginResult(true, "success");
            } else {
                return new LoginResult(false, "неверный пароль");
            }
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public void logout() {
        loginService.logout();
    }

    @RequestMapping("/current")
    public User currentUser() {
        return loginService.getUser();
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public LoginResult register(@RequestBody RegistrationRequest request) {
        User usr = userRepository.findByEmail(request.getEmail());
        if (usr != null) {
            return new LoginResult(false, "Такой пользователь уже существует");
        }
        if (StringUtils.isBlank(request.getUserName()) || StringUtils.isBlank(request.getPassword())) {
            return new LoginResult(false, "Пожалуйста, заполните ВСЕ поля");
        }
        if (request.getPassword().length() < 8) {
            return new LoginResult(false, "Длина пароля должна быть не менее 8 символов");
        }

        User user = new User();
        user.setUserName(request.getUserName());
        user.setEmail(request.getEmail());
        user.setPassword(request.getPassword());
        user.setPhone(request.getPhone());
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        try {
            user.setBirthDate(format.parse(request.getBirthDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        userRepository.save(user);
        loginService.login(user);
        return new LoginResult(true, "");
    }
}
