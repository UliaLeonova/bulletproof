package com.bulletproof.user;

import com.bulletproof.basket.domain.Basket;
import com.bulletproof.user.domain.User;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

@Service
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class LoginService {
    private User user;
    private Basket basket = new Basket();

    public void login(User user) {
        this.user = user;
        if (basket == null) {
            basket = new Basket();
        }
    }

    public void logout() {
        this.user = null;
        this.basket = null;
    }

    public Basket getBasket() {
        return basket;
    }

    public User getUser() {
        return user;
    }

    public String getCurrentUser() {
        if (user != null) {
            return user.getUserName();
        } else {
            return null;
        }
    }

}
