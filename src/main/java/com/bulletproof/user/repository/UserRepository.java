package com.bulletproof.user.repository;

import com.bulletproof.user.domain.User;
import org.springframework.data.repository.CrudRepository;


public interface UserRepository extends CrudRepository<User, Long> {

    User findByEmail(String email);
}
