package com.bulletproof.product.repository;

import com.bulletproof.category.domain.Category;
import com.bulletproof.product.domain.Product;
import com.bulletproof.product.domain.ProductSize;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    List<Product> findByCategories_IdAndCreatedDateGreaterThanEqual(Long categoryId, LocalDateTime createdDate, Pageable pageable);
    List<Product> findByCategories_IdAndSalePriceIsNotNull(Long categoryId, Pageable pageable);
    List<Product> findByCategories_IdIn(List<Long> categoryIds, Pageable pageable);
    Page<Product> findAll(Pageable pageable);

    List<Product> findByCreatedDateGreaterThanEqual(LocalDateTime createdDate, Pageable pageable);
    List<Product> findBySalePriceIsNotNull(Pageable pageable);

    List<Product> findByCategories_IdAndCreatedDateGreaterThanEqualAndSizeEquals(Long categoryId, LocalDateTime createdDate, ProductSize size, Pageable pageable);
    List<Product> findByCategories_IdAndSalePriceIsNotNullAndSizeEquals(Long categoryId, ProductSize size, Pageable pageable);
    List<Product> findByCategories_IdInAndSalePriceIsNotNullAndSizeEquals(List<Long> categoryIds, ProductSize size, Pageable pageable);
    List<Product> findByCategories_IdInAndSizeEquals(List<Long> categoryIds, ProductSize size,Pageable pageable);

    List<Product> findByCreatedDateGreaterThanEqualAndSizeEquals(LocalDateTime createdDate, ProductSize size, Pageable pageable);
    List<Product> findBySalePriceIsNotNullAndSizeEquals( ProductSize size, Pageable pageable);
    List<Product> findBySize(ProductSize size, Pageable pageable);
}
