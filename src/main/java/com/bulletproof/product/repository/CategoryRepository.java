package com.bulletproof.product.repository;

import com.bulletproof.category.domain.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {

    List<Category> findByCategoryType(Category.CategoryType type);
}
