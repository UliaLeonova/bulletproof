package com.bulletproof.product.service;

import com.bulletproof.product.domain.Product;
import com.bulletproof.product.domain.ProductSize;
import com.bulletproof.product.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Created by xQwert on 12/6/2015.
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;


    @Override
    public List<Product> getProducts() {

        return StreamSupport.stream(productRepository.findAll().spliterator(), false).collect(Collectors.toList());
    }


    @Override
    public List<Product> getProductsForDate(Long categoryId, LocalDateTime creationDate, String sortBy, String sortOrder, String filter, Integer pageSize, Integer pageNum) {
       Pageable pageable = getPageable(sortBy, sortOrder, pageSize, pageNum);
        if (filter == null) {
            if (categoryId != null) {
                return productRepository.findByCategories_IdAndCreatedDateGreaterThanEqual(categoryId, creationDate, pageable);
            } else {
                return productRepository.findByCreatedDateGreaterThanEqual(creationDate, pageable);
            }
        } else {
            if (categoryId != null) {
                return productRepository.findByCategories_IdAndCreatedDateGreaterThanEqualAndSizeEquals(categoryId,
                        creationDate,
                        ProductSize.valueOf(filter),
                        pageable);
            } else {
                return productRepository.findByCreatedDateGreaterThanEqualAndSizeEquals(creationDate,
                        ProductSize.valueOf(filter),
                        pageable);
            }
        }

    }

    @Override
    public List<Product> getSaleProducts(Long categoryId, String sortBy, String sortOrder, String filter, Integer pageSize, Integer pageNum) {
        Pageable pageable = getPageable(sortBy, sortOrder, pageSize, pageNum);
        if (filter == null) {
            if (categoryId != null) {
                return productRepository.findByCategories_IdAndSalePriceIsNotNull(categoryId, pageable);
            } else {
                return productRepository.findBySalePriceIsNotNull(pageable);
            }
        } else {
            if (categoryId != null) {
                return productRepository.findByCategories_IdAndSalePriceIsNotNullAndSizeEquals(categoryId,
                        ProductSize.valueOf(filter),
                        pageable);
            } else {
                return productRepository.findBySalePriceIsNotNullAndSizeEquals(
                        ProductSize.valueOf(filter),
                        pageable);
            }
        }
    }

    @Override
    public List<Product> getProducts(List<Long> categoryIds, String sortBy, String sortOrder, String filter, Integer pageSize, Integer pageNum) {


        Pageable pageable = getPageable(sortBy, sortOrder, pageSize, pageNum);
        if (filter == null) {
            if (categoryIds != null && !categoryIds.isEmpty()) {
                return productRepository.findByCategories_IdIn(categoryIds, pageable);
            } else {
                Page<Product> page = productRepository.findAll(pageable);
                return page.getContent();
            }
        } else {
            if (categoryIds != null && !categoryIds.isEmpty()) {
                return productRepository.findByCategories_IdInAndSizeEquals(categoryIds,
                        ProductSize.valueOf(filter),
                        pageable);
            } else {
                return productRepository.findBySize(
                        ProductSize.valueOf(filter),
                        pageable);
            }
        }
    }

    @Override
    public Product findProduct(Long id) {
        return productRepository.findOne(id);
    }

    private Pageable getPageable(String sortBy, String sortOrder, Integer pageSize, Integer pageNum) {
        Sort sort = null;
        if (sortBy == null) {
            sortBy = "title";
        }
        if (sortOrder == null) {
            sortOrder = "ASC";
        }
        sort = new Sort(Sort.Direction.fromString(sortOrder), sortBy);
        if (pageNum == null ) {
            pageNum = 0;
        }

        if (pageSize == null) {
            pageSize = 15;
        }

        return new PageRequest(pageNum,pageSize, sort);

    }
}
