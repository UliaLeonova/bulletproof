package com.bulletproof.product.service;

import com.bulletproof.category.domain.Category;
import com.bulletproof.product.domain.Product;
import org.springframework.data.domain.Sort;

import java.time.LocalDateTime;
import java.util.List;

public interface ProductService {

    List<Product> getProducts();

    List<Product> getProductsForDate(Long categoryId,
                                     LocalDateTime creationDate,
                                     String sortBy,
                                     String sortOrder,
                                     String filter,
                                     Integer pageSize,
                                     Integer pageNum
    );

    List<Product> getSaleProducts(Long categoryId,
                                  String sortBy,
                                  String sortOrder,
                                  String filter,
                                  Integer pageSize,
                                  Integer pageNum
    );

    List<Product> getProducts(List<Long> categoryIds,String sortBy,
                              String sortOrder,
                              String filter,
                              Integer pageSize,
                              Integer pageNum);

    Product findProduct(Long id);
}
