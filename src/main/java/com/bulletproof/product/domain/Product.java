package com.bulletproof.product.domain;

import com.bulletproof.category.domain.Category;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by xQwert on 12/6/2015.
 */
@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToMany
    private List<Category> categories;

    private String title;
    @Enumerated(EnumType.STRING)
    private ProductSize size;

    private Double price;
    private Double salePrice;
    private String material;
    private String counrty;
    private String description;

    @ElementCollection
    private List<String> images;

    @Column(columnDefinition = "timestamp default now()")
    private LocalDateTime createdDate;




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ProductSize getSize() {
        return size;
    }

    public void setSize(ProductSize size) {
        this.size = size;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getCounrty() {
        return counrty;
    }

    public void setCounrty(String counrty) {
        this.counrty = counrty;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
