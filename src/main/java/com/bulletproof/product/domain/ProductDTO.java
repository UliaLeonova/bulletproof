package com.bulletproof.product.domain;

import com.bulletproof.category.domain.Category;

import javax.persistence.ElementCollection;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import java.util.List;
import java.util.stream.Collectors;

public class ProductDto {

    private Long id;
    private List<String> categories;
    private String title;
    private String size;

    private Double price;
    private Double salePrice;
    private String material;
    private String counrty;
    private String description;

    private List<String> images;

    public ProductDto() {
    }
    public ProductDto(Product product) {
        id = product.getId();
        categories = product.getCategories().stream().map(Category::getTitle).collect(Collectors.toList());
        title = product.getTitle();
        size = product.getSize().name();
        price = product.getPrice();
        salePrice = product.getSalePrice();
        material = product.getMaterial();
        counrty = product.getCounrty();
        description = product.getDescription();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getCounrty() {
        return counrty;
    }

    public void setCounrty(String counrty) {
        this.counrty = counrty;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
