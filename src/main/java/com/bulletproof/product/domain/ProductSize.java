package com.bulletproof.product.domain;

public enum ProductSize {

    XS,S,M,L
}
