package com.bulletproof.product.controller;

import com.bulletproof.category.domain.Category;
import com.bulletproof.product.domain.Product;
import com.bulletproof.product.domain.ProductSize;
import com.bulletproof.product.repository.CategoryRepository;
import com.bulletproof.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private CategoryRepository categoryRepository;

    @RequestMapping("/{rootCategory}")
    public List<Product> listOfProducts(@PathVariable Long rootCategory,
                                        @RequestParam(required = false) Long categoryId,
                                        @RequestParam(required = false) String sortBy,
                                        @RequestParam(required = false) String sortOrder,
                                        @RequestParam(required = false) String filter,
                                        @RequestParam(required = false) Integer pageSize,
                                        @RequestParam(required = false) Integer pageNum) {
        List<Long> categories = new ArrayList<>();
        categories.add(rootCategory);
        if (categoryId != null) {
            categories.add(categoryId);
        }
        return productService.getProducts(categories, sortBy, sortOrder, filter, pageSize, pageNum);
    }

    @RequestMapping("/")
    public List<Product> listOfAllProducts(
                                        @RequestParam(required = false) Long categoryId,
                                        @RequestParam(required = false) String sortBy,
                                        @RequestParam(required = false) String sortOrder,
                                        @RequestParam(required = false) String filter,
                                        @RequestParam(required = false) Integer pageSize,
                                        @RequestParam(required = false) Integer pageNum) {
        List<Long> categories = new ArrayList<>();
        if (categoryId != null) {
            categories.add(categoryId);
        }
        return productService.getProducts(categories, sortBy, sortOrder, filter, pageSize, pageNum);
    }

    @RequestMapping("/new")
    public List<Product> listOfNewProducts(@RequestParam(required = false) Long categoryId,
                                           @RequestParam(required = false) String sortBy,
                                           @RequestParam(required = false) String sortOrder,
                                           @RequestParam(required = false) String filter,
                                           @RequestParam(required = false) Integer pageSize,
                                           @RequestParam(required = false) Integer pageNum
    ) {
        LocalDateTime time = LocalDateTime.now().minusMonths(1);
        return productService.getProductsForDate(categoryId,time, sortBy, sortOrder, filter, pageSize, pageNum);
    }

    @RequestMapping("/sale")
    public List<Product> listOfSaleProducts(@RequestParam(required = false) Long categoryId,
                                           @RequestParam(required = false) String sortBy,
                                           @RequestParam(required = false) String sortOrder,
                                           @RequestParam(required = false) String filter,
                                           @RequestParam(required = false) Integer pageSize,
                                           @RequestParam(required = false) Integer pageNum
    ) {
        return productService.getSaleProducts(categoryId,sortBy, sortOrder, filter, pageSize, pageNum);
    }

    @RequestMapping("/size")
    public List<String> getFilterList() {
        return Stream.of(ProductSize.values())
                .map(Enum::name)
                .collect(Collectors.toList());
    }



    @RequestMapping("/category")
    public List<Category> getAllCategories(
            @RequestParam(required = false, defaultValue = "PARENT") Category.CategoryType type) {

        return categoryRepository.findByCategoryType(type);
    }
}
