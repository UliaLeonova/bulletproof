package com.bulletproof.category.domain;

import javax.persistence.*;

/**
 * Created by xQwert on 12/6/2015.
 */
@Entity
public class Category {
    @Id
    @GeneratedValue
    private Long id;

    private String title;

    @ManyToOne
    private Category parentCategory;

    @Enumerated(EnumType.STRING)
    private CategoryType categoryType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Category parentCategory) {
        this.parentCategory = parentCategory;
    }

    public CategoryType getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(CategoryType categoryType) {
        this.categoryType = categoryType;
    }

    public enum CategoryType {
        PARENT, COLLECTION, SALE, ACTION
    }
}
